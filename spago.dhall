{-
Welcome to a Spago project!
You can edit this file as you like.
-}
{ name = "process"
, dependencies =
  [ "console"
  , "debug"
  , "dotlang"
  , "effect"
  , "formatters"
  , "halogen"
  , "now"
  , "numbers"
  , "psci-support"
  , "svg-parser-halogen"
  ]
, packages = ./packages.dhall
, sources = [ "src/**/*.purs", "test/**/*.purs" ]
}
