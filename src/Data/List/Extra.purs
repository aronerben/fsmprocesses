module Data.List.Extra where

import Processes.AppPrelude (class Eq, not, (&&))
import Data.List (List(..), elem)

disjoint :: forall a. Eq a => List a -> List a -> Boolean
disjoint a Nil = true

disjoint Nil b = true

disjoint (Cons a as) b = not (a `elem` b) && disjoint as b

isSubsequenceOf :: forall a. Eq a => List a -> List a -> Boolean
isSubsequenceOf Nil b = true

isSubsequenceOf (Cons a as) b = a `elem` b && isSubsequenceOf as b
