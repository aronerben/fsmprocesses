module Processes.Model.Types where

import Data.List (List)
import Data.Eq (class Eq)
import Data.Map as MS
import Data.Ord (class Ord)
import Data.Show (class Show)
import Processes.Form.Types (FormData, FormDefinition, FormEntry)

-- | The name of a form field.
type FieldName
  = String

-- | Typed global variable assignment with history.
type Data
  = MS.Map FieldName (List FormEntry)

-- | Global meta data, meta data per transition (identified with name)
type Meta
  = MS.Map TransitionId (MS.Map MetaKey String)

-- | State of the finite state machine.
type State
  = String

-- | Name of a process
type ProcessName
  = String

-- | A unique name for each state transition in the finite state machine.
type TransitionId
  = String

-- | A role is a tag which can be assigned to a user for permission purposes.
type RoleTag
  = String

-- | A unique name for a user interacting with a process.
type UserId
  = String

-- | A priority for a bot, the higher the number, the lower the priority
type Priority
  = Int

-- | A user with roles
type User
  = { userId :: UserId
    , userRoles :: List RoleTag
    , bots :: List Bot
    }

-- | A bot, can act on behalf of a user if constraints are fulfilled
type Bot
  = { transitionId :: TransitionId
    , formData :: FormData
    , constraints :: List Constraint
    , priority :: Priority
    }

-- | User submitted form, the form data provided by a user when attempting to execute a transition.
-- | for example a User has filled out a form X
-- | - `sender`:
-- | - `formData`
type Commit
  = { sender :: User
    , transition :: TransitionId
    , formData :: FormData
    }

-- | All stuff the user needs to execute a given transition.
-- |
-- | `formDefinition` The form definition to render
-- | Think: User Dandolo must fill out fields name and address, and can optionally provide his birthday
-- Todo: Generated from 'requiredFields' and 'optionalFields' in ValidationData
-- `rRequired` Fields that need to be submitted.
-- `rOptional` Optional additional fields.
-- when a Request is being built to send to the user.
type Request
  = { formDefinition :: FormDefinition
    } -- rRequired :: List FieldName -- , rOptional :: List FieldName

type Error
  = String

-- | Description of the data needed to check whether the transition can
-- | be executed.
-- |
-- | - `checkRoles` Roles that have permission to submit the action.
-- | - `requiredFields` Form fields that are required to go on.
-- | - `optionalFields` Additional optional form fields.
-- | - `prerequiredFields` Fields submitted in a previous step necessary to initiate the transition.
-- | - `constraints` Constraints on data and meta data
type ValidationData
  = { checkRoles :: List RoleTag
    , requiredFields :: List FieldName
    , optionalFields :: List FieldName
    , prerequiredFields :: List FieldName
    , constraints :: List Constraint
    }

-- | A transition has an `origin` and an `exit`. It describes something
-- | like `A -> B`, where the the arrow holds the `request`, validation` and `meta`
type Transition
  = { origin :: State
    , exit :: State
    , request :: Request
    , validation :: ValidationData
    , meta :: List MetaKey
    }

type Transitions
  = MS.Map TransitionId Transition

-- | Describes a momentary state of the process.
-- | - `currentState` holds the state of the finite state machine.
-- | - `transitions` holds all possible transitions of the state machine.
-- | - `data` Global variable assignment with history.
-- | - `meta` Global meta data.
type Snapshot
  = { currentState :: State
    , transitions :: Transitions
    , data :: Data
    , meta :: Meta
    }

-- | All Data regarding a process.
-- |
-- | - `snapshot` is the current state of the process.
-- | - `name` is the name of the process.
-- | - `users` are a list of participants in this process.
type Process
  = { snapshot :: Snapshot
    , name :: ProcessName
    , users :: List User
    }

-- | A single meta data value
-- |
-- | - `Initiator` is the transitions intiator.
-- | - `InitiationDate` is the date the transition happend.
data MetaKey
  = Initiator
  | InitiationDate

instance showMetaKey :: Show MetaKey where
  show Initiator = "Initiator"
  show InitiationDate = "InitiationDate"

derive instance ordMetaKey :: Ord MetaKey

derive instance eqMetaKey :: Eq MetaKey

-- | A single constraint
-- |
-- | - `MetaConstraint` is a constraint on meta data values.
-- | - `DataConstraint` is a constraint on form data values.
-- | - `key` represents the different key types for both constraint types.
data Constraint
  = MetaConstraint { key :: MetaKey | ValueCompare }
  | DataConstraint { key :: FieldName | ValueCompare }

derive instance eqConstraint :: Eq Constraint

derive instance eqComparator :: Eq Comparator

derive instance eqConstraintValue :: Eq ConstraintValue

-- | A general value with comparator
-- |
-- | - `comparator` is the comparator element, LT/GT/EQ, etc.
-- | - `value` is the value that the data value should be compared to.
type ValueCompare
  = ( comparator :: Comparator
    , value :: ConstraintValue
    )

-- | A constraint value to be compared to
-- |
-- | - `Literal` is a string.
-- | - `MKey` is the key of the meta data that should give the constrain value, given a transition name.
-- | - `DKey` is the key data that should give the constrain value.
data ConstraintValue
  = Literal String
  | MKey TransitionId MetaKey
  | DKey FieldName

-- | A comparator
-- | Cannot use Ordering because of NotEqual requirement
-- |
-- | - `Equal` for equality.
-- | - `NotEqual` for non-equality.
-- | - `Greater` for > greater.
-- | - `Less` for < less.
data Comparator
  = Equal
  | NotEqual
  | Greater
  | Less
