module Processes.Model.UpdateSystem where

import Control.MonadZero (guard)
import Data.DateTime (DateTime)
import Data.Either (Either(..))
import Data.Eq (class Eq)
import Data.Foldable (all, elem, minimumBy)
import Data.Formatter.DateTime (formatDateTime)
import Data.List (List, null, singleton, union, head, filter)
import Data.List.Extra (disjoint, isSubsequenceOf)
import Data.Map as MS
import Data.Maybe (Maybe(..), fromMaybe)
import Data.Ord (class Ord)
import Data.Set as Set
import Data.Tuple (Tuple(..))
import Processes.AppPrelude (compare, (<<<), bind, discard, map, not, pure, when, (#), ($), (&&), (<$>), (<>), (==), (>), (<))
import Processes.Form.Render (entryToText)
import Processes.Form.Types (FormData(..), FormEntry, NamedFormEntry, unFormData)
import Processes.Model.Types (State, Commit, Comparator(..), Constraint(..), ConstraintValue(..), Data, Error, FieldName, Meta, MetaKey(..), Request, RoleTag, Snapshot, Transition, TransitionId, Transitions, User, UserId)

-- | Get currently available transitions.
scan :: Snapshot -> User -> List TransitionId
scan snapshot user = ks filtered
  where
  transitions :: Transitions
  transitions = snapshot.transitions

  filtered :: MS.Map TransitionId Transition
  filtered =
    MS.filter
      ( \(transition :: Transition) ->
          snapshot.currentState == transition.origin
            && not (transition.validation.checkRoles `disjoint` user.userRoles)
      )
      transitions

  ks :: MS.Map TransitionId Transition -> List TransitionId
  ks s = MS.keys s # Set.toUnfoldable

-- | Get information about required input for a given transition.
getRequest :: TransitionId -> Snapshot -> Either Error Request
getRequest trId sn = case tr of
  Nothing -> Left $ "Transition not found"
  Just transition -> Right transition.request
  where
  tr = MS.lookup trId $ sn.transitions

-- | Verify if a commit is suitable to execute a given transition
-- | * check if all necessary input is provided
-- | * check if commiting user has right to execute transition
-- | if verification is succesfull, return new snapshot, otherwise
-- | return error message
updateCommit :: Commit -> Snapshot -> DateTime -> List User -> Either Error Snapshot
updateCommit c sn dt us = do
  valData <- _.validation <$> tr
  metaRequired <- map _.meta tr
  when
    (valData.checkRoles `disjoint` senderRoles)
    (Left "Transition denied: user access denied")
  let
    admissibleCommitData = valData.requiredFields <> valData.optionalFields

    newMeta = updateMetaData metaRequired sn.meta

    newData = updateData input sn.data
  when
    ( not (valData.requiredFields `isSubsequenceOf` inputFields)
    )
    (Left $ "Transition denied: data missing in commit")
  when
    (not (inputFields `isSubsequenceOf` admissibleCommitData))
    (Left "Transition denied: excess data in commit")
  when
    (not (valData.prerequiredFields `isSubsequenceOf` stateData))
    (Left "Transition denied: data missing in state")
  when
    (not (all (satisfied c.transition newMeta newData) valData.constraints))
    (Left "Transition denied: constraints failed")
  newState <- map _.exit tr
  runBots us dt
    $ sn { currentState = newState, meta = newMeta, data = newData }
  where
  -- Set meta data and merge
  updateMetaData :: List MetaKey -> Meta -> Meta
  updateMetaData metaReq oldMeta =
    MS.union
      ( MS.fromFoldable
          [ ( Tuple c.transition
                $ MS.fromFoldable
                $ map assocMeta metaReq
            )
          ]
      )
      oldMeta
    where
    -- Associate meta keys with the values that have to be set
    assocMeta :: MetaKey -> Tuple MetaKey String
    assocMeta Initiator = Tuple Initiator c.sender.userId

    assocMeta InitiationDate = Tuple InitiationDate $ convertDateTimeReadable dt
      where
      convertDateTimeReadable :: DateTime -> String
      convertDateTimeReadable dateTime = case formatDateTime "YYYY-MM-DD HH:mm:ss" dateTime of
        -- If time format conversion fails, use the failed string
        -- No timezone conversion yet either
        -- Can be improved
        Left fail -> fail
        Right succ -> succ

  senderId :: UserId
  senderId = c.sender.userId

  senderRoles :: List RoleTag
  senderRoles = c.sender.userRoles

  input :: FormData
  input = c.formData

  inputFields :: List FieldName
  inputFields = map _.fieldName (unFormData input).fields

  currentState :: String
  currentState = sn.currentState

  tr :: Either String Transition
  tr = case MS.lookup c.transition sn.transitions of
    Nothing -> Left $ "Transition not found"
    Just tr' -> Right $ tr'

  stateData :: List String
  stateData =
    Set.toUnfoldable $ MS.keys
      $ MS.filter (\d -> not (null d))
      $ sn.data

  -- | Takes the existing data and adds the new data.
  updateData :: FormData -> Data -> Data
  updateData fData prevData = mergeData prevData (newData fData)
    where
    newData :: FormData -> MS.Map FieldName (List FormEntry)
    newData (FormData formData) =
      MS.fromFoldable
        $ map
            (\(namedFormEntry :: NamedFormEntry) -> Tuple namedFormEntry.fieldName (singleton namedFormEntry.entry))
            formData.fields

    -- mergeData :: Map a [b] -> Map a b' -> Map a [b':b]
    -- intersectionWith :: forall k a b c. Ord k => (a -> b -> c) -> Map k a -> Map k b -> Map k c
    mergeData :: Data -> MS.Map FieldName (List FormEntry) -> Data
    mergeData = MS.unionWith (\a b -> union a b)

runBots :: List User -> DateTime -> Snapshot -> Either Error Snapshot
runBots us dt sn = case constructCommit of
  -- No bot commit found => update snapshot
  Nothing -> pure sn
  -- Bot commit found => Do all checks recursively (next step might contain bot action again)
  -- Is tail-recursive
  Just cmt -> updateCommit cmt sn dt us
  where
  -- A bot is runnable when correct transition and constraint fulfilled
  -- Picks first user that has bots that can be run. Picks bot with highest priority from that user.
  -- Directly constructs Commit
  constructCommit :: Maybe Commit
  constructCommit = do
    let
      -- Get all users that have bots designated to the transition that is now possible for that user (roles checking & constraint checking)
      validUsersWithBots =
        filter (not <<< null <<< _.bots)
          $ map
              ( \user ->
                  user
                    { bots =
                      filter
                        ( \bot ->
                            (elem bot.transitionId $ scan sn user)
                              && all (satisfied bot.transitionId sn.meta sn.data) bot.constraints
                        )
                        user.bots
                    }
              )
              us
    -- Pick first user
    -- Can be improved by utilizing clear first user pick (alphabetical?)
    firstUser <- head validUsersWithBots
    -- Get bot with lowest priority
    -- If lowest priority is duplicated, behaviour not well defined
    -- Can be improved by utilizing clear pick if lowest priority is duplicated
    bot <- minimumBy (\a b -> compare a.priority b.priority) firstUser.bots
    pure { sender: firstUser, transition: bot.transitionId, formData: bot.formData }

-- Constraints fail if no meta entry or no data entry for keys provided
satisfied :: TransitionId -> Meta -> Data -> Constraint -> Boolean
satisfied tId meta dat con = fromMaybe false $ check tId meta dat con

-- Constraint type semantics
check :: TransitionId -> Meta -> Data -> Constraint -> Maybe Boolean
check tId meta dat (MetaConstraint { key: key, comparator: comp, value: val }) = do
  cur <- lookupTransitionMetaValue meta tId key
  compareVals meta dat cur val comp

check tId meta dat (DataConstraint { key: key, comparator: comp, value: val }) = do
  cur <- lookupDataValue dat key
  compareVals meta dat cur val comp

-- Constraint value semantics
compareVals :: Meta -> Data -> String -> ConstraintValue -> Comparator -> Maybe Boolean
compareVals meta dat cur (Literal b) comp = pure $ eval comp cur b

compareVals meta dat cur (MKey transition mk) comp = do
  val <- lookupTransitionMetaValue meta transition mk
  pure $ eval comp cur val

compareVals meta dat cur (DKey dk) comp = do
  val <- lookupDataValue dat dk
  pure $ eval comp cur val

-- Comparator semantics
eval :: forall a. Eq a => Ord a => Comparator -> a -> a -> Boolean
eval Equal a b = a == b

eval NotEqual a b = not (a == b)

eval Greater a b = a > b

eval Less a b = a < b

lookupTransitionMetaValue :: Meta -> TransitionId -> MetaKey -> Maybe String
lookupTransitionMetaValue meta tid mk = do
  transition <- MS.lookup tid meta
  res <- MS.lookup mk transition
  pure $ res

lookupDataValue :: Data -> FieldName -> Maybe String
lookupDataValue dat dk = do
  res <- MS.lookup dk dat
  head $ map entryToText res
