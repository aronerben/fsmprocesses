module Processes.Samples.Hardware where

import Data.List (fromFoldable)
import Data.Map as MS
import Data.Tuple (Tuple(..))
import Processes.AppPrelude (($))
import Processes.Form.Types (FormEntry(..), BasicValue(..), FormData(..), FormDefinition(..), FormEntryDefinition(..), FormEntryDefinitionDynamic(..), FormEntryDefinitionFixed(..))
import Processes.Model.Types (Comparator(..), Constraint(..), ConstraintValue(..), MetaKey(..), Process, Snapshot, Transition, Transitions, User)

process :: Process
process =
  { snapshot: initialSnapshot
  , name: "Hardware Process"
  , users: fromFoldable [ defaultUser, bossUser ]
  }

defaultUser :: User
defaultUser =
  { userId: "dandolo"
  , userRoles: fromFoldable [ "employee", "boss" ]
  , bots:
    fromFoldable
      [
      ]
  }

bossUser :: User
bossUser =
  { userId: "joris"
  , userRoles: fromFoldable [ "boss" ]
  , bots:
    fromFoldable
      [ { transitionId: "reviewHardware"
        , formData:
          FormData
            { fields:
              fromFoldable
                [ { fieldName: "confirmation"
                  , entry: Radio $ TText "yes"
                  }
                ]
            }
        , constraints:
          fromFoldable
            [ DataConstraint
                { key: "hardwareList"
                , comparator: NotEqual
                , value: Literal "huawei"
                }
            ]
        , priority: 1
        }
      , { transitionId: "reviewHardware"
        , formData:
          FormData
            { fields:
              fromFoldable
                [ { fieldName: "confirmation"
                  , entry: Radio $ TText "no"
                  }
                ]
            }
        , constraints:
          fromFoldable
            []
        , priority: 2
        }
      ]
  }

initialSnapshot :: Snapshot
initialSnapshot =
  { currentState: "root"
  , transitions: transitions
  , data: MS.fromFoldable []
  , meta: MS.fromFoldable []
  }

transitions :: Transitions
transitions =
  MS.fromFoldable
    [ Tuple "chooseHardware" root
    , Tuple "reviewHardware" reviewHardware
    ]

root :: Transition
root =
  { origin: "root"
  , exit: "hardwareChosen"
  , request:
    { formDefinition: requestForm
    }
  , validation:
    { checkRoles: fromFoldable [ "employee" ]
    , requiredFields: fromFoldable [ "hardwareList" ]
    , optionalFields: fromFoldable []
    , prerequiredFields: fromFoldable []
    , constraints: fromFoldable []
    }
  , meta: fromFoldable [ Initiator, InitiationDate ]
  }

reviewHardware :: Transition
reviewHardware =
  { origin: "hardwareChosen"
  , exit: "exit"
  , request:
    { formDefinition: isThisOkayForm
    }
  , validation:
    { checkRoles: fromFoldable [ "boss" ]
    , requiredFields: fromFoldable [ "confirmation" ]
    , optionalFields: fromFoldable []
    , prerequiredFields: fromFoldable []
    , constraints:
      fromFoldable
        [ MetaConstraint
            { key: Initiator
            , comparator: NotEqual
            , value: MKey "chooseHardware" Initiator
            }
        ]
    }
  , meta: fromFoldable [ Initiator, InitiationDate ]
  }

isThisOkayForm :: FormDefinition
isThisOkayForm =
  FormDefinition
    { entries:
      fromFoldable
        [ Fixed (Title "Please confirm this order")
        , Dynamic "confirmation" "Is this okay?"
            ( RadioDefinition
                $ fromFoldable
                    [ { label: "yes"
                      , value: TText "yes"
                      }
                    , { label: "no"
                      , value: TText "no"
                      }
                    ]
            )
        ]
    }

requestForm :: FormDefinition
requestForm =
  FormDefinition
    { entries:
      fromFoldable
        [ Fixed (Title "Hi, please submit this form. ")
        , Dynamic "hardwareList" "Hardware"
            ( RadioDefinition
                $ fromFoldable
                    [ { label: "Lenovo"
                      , value: TText "lenovo"
                      }
                    , { label: "Huawei"
                      , value: TText "huawei"
                      }
                    , { label: "Microsoft"
                      , value: TText "microsoft"
                      }
                    ]
            )
        ]
    }
