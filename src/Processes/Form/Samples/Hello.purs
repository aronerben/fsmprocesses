module Form.Samples.Hello where

import Processes.AppPrelude (($))
import Data.List (fromFoldable)
import Processes.Form.Types
  ( BasicValue(..)
  , FormData(..)
  , FormDefinition(..)
  , FormEntry(..)
  , FormEntryDefinition(..)
  , FormEntryDefinitionDynamic(..)
  , FormEntryDefinitionFixed(..)
  )

requestForm :: FormDefinition
requestForm =
  FormDefinition
    { entries:
      fromFoldable
        [ Fixed (Title "Hello Header")
        , Fixed (Paragraph "Hello paragraph")
        , Dynamic "field-1" "Number (Unit) Input Hello Label" (UnitNumberDefinition "number")
        , Dynamic "field-1-1" "Line Input Hello Label" (LineDefinition "placeholder")
        , Dynamic "field-1-2" "Line Input Hello Label 2" (LineDefinition "placeholder")
        , Dynamic "field-2" "Text Input Date Label" (DateDefinition)
        , Dynamic "field-3" "Multiline Hello Label" (MultiLineDefinition "multiline input")
        , Dynamic "field-4" "Radio Hello Label"
            ( RadioDefinition
                $ fromFoldable
                    [ { label: "radio label 1"
                      , value: TText "foo"
                      }
                    , { label: "radio label 2"
                      , value: TText "foo"
                      }
                    , { label: "radio label 3"
                      , value: TText "foo"
                      }
                    ]
            )
        , Dynamic "field-5" "Dropdown Hello Label"
            ( RadioDefinition
                $ fromFoldable
                    [ { label: "dropdown label 1"
                      , value: TText "foo"
                      }
                    , { label: "dropdown label 2"
                      , value: TText "foo"
                      }
                    , { label: "dropdown label 3"
                      , value: TText "foo"
                      }
                    ]
            )
        ]
    }

fdata :: FormData
fdata =
  FormData
    { fields:
      fromFoldable
        [ { fieldName: "field-1-1"
          , entry: Line "awesome content"
          }
        ]
    }
