module Processes.Form.Render where

import Processes.AppPrelude
  ( Unit, bind, discard, map, show, ($), (<$>), (<<<), (<>), (==), (>)
  )
import Data.Maybe (Maybe(..), fromMaybe)
import Processes.Form.Types
  ( BasicValue
  , FieldName
  , FormData(..)
  , FormDefinition
  , FormEntry(..)
  , FormEntryDefinition(..)
  , FormEntryDefinitionDynamic(..)
  , FormEntryDefinitionFixed(..)
  , LabeledValue
  , NamedFormEntry
  , unFormData
  , unFormDefinition
  )
import Data.Array ((:))
import Data.List (List, filter, find, fromFoldable, insert, length, toUnfoldable)
import Effect.Aff (Aff)
import Halogen as H
import Halogen.HTML as HH
import Halogen.HTML.Events as HE
import Halogen.HTML.Properties as HP
import Processes.UI (mkClass)

type Slot
  = H.Slot Query RenderState

data Query a
  = GetFormData (RenderState -> a)

data Action
  = UpdateRenderState RenderState

type UpdateData
  = FieldName -> FormEntry -> FormData

type RenderState
  = { formData :: FormData
    , formDefinition :: FormDefinition
    }

initialState :: { formData :: FormData, formDefinition :: FormDefinition } -> RenderState
initialState i =
  { formData: i.formData
  , formDefinition: i.formDefinition
  }

component :: H.Component HH.HTML Query { formData :: FormData, formDefinition :: FormDefinition } RenderState Aff
component =
  H.mkComponent
    { initialState: initialState
    , render: render'
    , eval:
      H.mkEval
        $ H.defaultEval
            { handleAction = handleAction
            , handleQuery = handleQuery
            }
    }

updateFormData :: FormData -> UpdateData
updateFormData (FormData fData) fieldName newEntry =
  let
    newEntry' :: NamedFormEntry
    newEntry' = { fieldName: fieldName, entry: newEntry }
  in
    case find (\f -> f.fieldName == fieldName) fData.fields of
      Just entry -> FormData $ { fields: map (\f -> if f.fieldName == fieldName then newEntry' else f) fData.fields }
      Nothing -> FormData $ { fields: insert newEntry' fData.fields }

render' :: RenderState -> H.ComponentHTML Action () Aff
render' state =
  HH.div
    []
    ( map
        (renderForm' state)
        (toUnfoldable (unFormDefinition state.formDefinition).entries)
    )

renderForm' :: RenderState -> FormEntryDefinition -> H.ComponentHTML Action () Aff
renderForm' state entry@(Dynamic fieldname _ _) = renderFormEntryDefinition state mData (updateFormData state.formData) entry
  where
  mData :: Maybe FormEntry
  mData = map (\s -> s.entry) mF

  mF :: Maybe NamedFormEntry
  mF = find (\f -> f.fieldName == fieldname) (unFormData state.formData).fields

renderForm' state entry@(Fixed _) = renderFormEntryDefinition state Nothing (updateFormData state.formData) entry

renderFormEntryDefinition ::
  forall a.
  RenderState -> (Maybe FormEntry) -> UpdateData -> FormEntryDefinition -> H.ComponentHTML Action a Aff
renderFormEntryDefinition state mSelection update (Dynamic fieldname label (MultiSelectDefinition labels)) =
  HH.div
    [ mkClass "form-group" ]
    ( ( HH.label
          [ mkClass "form-control-label"
          ]
          [ HH.text label ]
      )
        : ( toUnfoldable
              $ map
                  ( \labeledValue ->
                      HH.button
                        [ mkClass $ "list-group-item-action list-group-item"
                            <> ( fromMaybe ""
                                  $ map
                                      ( \formEntry -> case formEntry of
                                          MultiSelect selectedValues ->
                                            if length (filter (\(s :: BasicValue) -> labeledValue.value == s) selectedValues) > 0 then
                                              " active"
                                            else
                                              ""
                                          _ -> ""
                                      )
                                      mSelection
                              )
                        , HE.onClick
                            $ ( \_ ->
                                  Just
                                    $ UpdateRenderState
                                        ( state
                                            { formData =
                                              update
                                                fieldname
                                                (MultiSelect (insert labeledValue.value $ fromMaybe (fromFoldable []) entries)) -- todo: upsert to list if not in list, remove from list if in list
                                            }
                                        )
                              )
                        ]
                        [ HH.text labeledValue.label
                        ]
                  )
                  (labels :: List LabeledValue)
          )
    )
  where
  entries :: Maybe (List BasicValue)
  entries =
    map
      ( \formEntry -> case formEntry of
          MultiSelect selectedValues -> selectedValues
          _ -> fromFoldable []
      )
      mSelection

renderFormEntryDefinition state mSelected update (Dynamic fieldname label (RadioDefinition labels)) =
  HH.div
    [ mkClass "form-group" ]
    ( ( HH.label
          [ mkClass "form-control-label"
          ]
          [ HH.text label ]
      )
        : ( toUnfoldable
              $ map
                  ( \labeledValue ->
                      HH.button
                        [ mkClass $ "list-group-item-action list-group-item"
                            <> ( if (Just $ Radio labeledValue.value) == mSelected then
                                  " active"
                                else
                                  ""
                              )
                        , HE.onClick
                            $ ( \_ ->
                                  Just
                                    $ UpdateRenderState
                                        ( state
                                            { formData =
                                              update
                                                fieldname
                                                (Radio labeledValue.value)
                                            }
                                        )
                              )
                        ]
                        [ HH.text labeledValue.label
                        ]
                  )
                  (labels :: List LabeledValue)
          )
    )

renderFormEntryDefinition state mState update (Dynamic fieldname label (LineDefinition placeholder)) =
  HH.div
    [ mkClass "form-group" ]
    [ HH.label [ mkClass "form-control-label" ] [ HH.text label ]
    , HH.input
        [ mkClass "form-control"
        , HP.type_ $ unitToInputType "text"
        , HP.placeholder placeholder
        , HP.value $ fromMaybe "" $ map entryToText mState
        , HE.onValueChange $ (\val -> Just $ UpdateRenderState (state { formData = update fieldname (Line val) }))
        ]
    ]

renderFormEntryDefinition state mState update (Dynamic fieldname label (MultiLineDefinition placeholder)) =
  HH.div
    [ mkClass "form-group" ]
    [ HH.label [ mkClass "form-control-label" ] [ HH.text label ]
    , HH.input
        [ mkClass "form-control"
        , HP.type_ $ unitToInputType "textarea"
        , HP.placeholder placeholder
        , HP.value $ fromMaybe "" $ map entryToText mState
        , HE.onValueChange $ (\val -> Just $ UpdateRenderState (state { formData = update fieldname (MultiLine val) }))
        ]
    ]

renderFormEntryDefinition state mState update (Dynamic fieldname label DateDefinition) =
  HH.div
    [ mkClass "form-group" ]
    [ HH.label [ mkClass "form-control-label" ] [ HH.text label ]
    , HH.input
        [ mkClass "form-control"
        , HP.type_ $ unitToInputType "date"
        , HP.value $ fromMaybe "" $ map entryToText mState
        , HE.onValueChange $ (\val -> Just $ UpdateRenderState (state { formData = update fieldname (Line val) })) -- todo: Date instead of Line
        ]
    ]

renderFormEntryDefinition state mState update (Dynamic fieldname label (UnitNumberDefinition unit)) =
  HH.div
    [ mkClass "form-group" ]
    [ HH.label [ mkClass "form-control-label" ] [ HH.text label ]
    , HH.input
        [ mkClass "form-control"
        , HP.type_ $ unitToInputType "number"
        , HP.value $ fromMaybe "" $ map entryToText mState
        , HE.onValueChange $ (\val -> Just $ UpdateRenderState (state { formData = update fieldname (Line val) })) -- todo: Number instead of Line
        ]
    ]

renderFormEntryDefinition state mState update (Fixed (Title title)) =
  HH.h2
    []
    [ HH.text title ]

renderFormEntryDefinition state mState update (Fixed (Paragraph p)) =
  HH.p
    []
    [ HH.text p ]

-- https://pursuit.purescript.org/packages/purescript-dom-indexed/7.0.0/docs/DOM.HTML.Indexed.InputType#t:InputType
unitToInputType :: String -> HP.InputType
unitToInputType "text" = HP.InputText

unitToInputType "number" = HP.InputNumber

unitToInputType "textarea" = HP.InputText

unitToInputType "date" = HP.InputDate

unitToInputType "radio" = HP.InputRadio

unitToInputType _ = HP.InputText

handleAction :: forall a b c. Action -> H.HalogenM RenderState c b RenderState a Unit
handleAction = case _ of
  (UpdateRenderState state) -> do
    H.put state
    currentState <- H.get
    H.raise currentState

handleQuery :: forall p o m a. Query a -> H.HalogenM RenderState Action p o m (Maybe a)
handleQuery = case _ of
  GetFormData state -> Just <<< state <$> H.get

entryToText :: FormEntry -> String
entryToText (Line v) = v

entryToText (Radio v) = show v

entryToText (MultiLine v) = v

entryToText (MultiSelect v) = show v

entryToText (Date v) = show v

entryToText (Number v) = show v
