module Processes.AppPrelude
  ( (<|)
  , (|>)
  , module Prelude
  ) where

import Prelude
import Data.Function (applyFlipped)

infixr 0 apply as <|

infixl 1 applyFlipped as |>
