module Processes.Graphviz where

import Processes.AppPrelude (map, ($), (<<<), (<>), (==), (|>))
import Color as Color
import Control.Bind (bindFlipped)
import Data.Array (intercalate)
import Data.Array as Array
import Data.DotLang (Definition(..), Graph(..), node, (=*>), (==>))
import Data.DotLang.Attr.Edge as Edge
import Data.DotLang.Attr.Global as Global
import Data.DotLang.Attr.Node as Node
import Data.DotLang.Class (toText)
import Data.List as List
import Data.Map (mapMaybeWithKey)
import Data.Map as MS
import Data.Maybe (Maybe(..))
import Data.Set as Set
import Data.String (Replacement(..), replaceAll)
import Data.String.Pattern as P
import Data.Tuple (Tuple(..))
import Effect.Aff (Aff)
import Effect.Aff.Compat (EffectFnAff, fromEffectFnAff)
import Processes.Model.Types
  ( Snapshot
  , State
  , Transition
  , TransitionId
  , Transitions
  , RoleTag
  , FieldName
  )

foreign import _viz_internal ∷ String -> String -> String -> EffectFnAff String

renderToSvg ∷ String -> Aff String
renderToSvg = fromEffectFnAff <<< (_viz_internal "dot" "svg")

-- type Node
snapshotToGraph :: Snapshot -> String
snapshotToGraph snapshot = toText graph
  where
  graph =
    DiGraph
      ( Global [ Global.RankDir Global.FromLeft ]
          Array.: transitionNodes
          <> nodes
          <> all
      )

  nodes :: Array Definition
  nodes =
    snapshot.transitions
      |> MS.values
      |> bindFlipped
          ( \transition ->
              List.fromFoldable [ transition.origin, transition.exit ]
          )
      |> Set.fromFoldable
      |> Array.fromFoldable
      |> map
          ( \unescapedNodeName ->
              let
                nodeName = escape unescapedNodeName
              in
                node nodeName
                  [ Node.FontColor (colorForNodeName nodeName)
                  , Node.Shape Node.Ellipse
                  ]
          )

  transitionNodes :: Array Definition
  transitionNodes =
    snapshot.transitions
      |> MS.toUnfoldable
      |> map
          ( \(Tuple transitionId transition) ->
              node (escape transitionId)
                [ Node.FontSize 8
                , Node.Margin 0
                , Node.Label
                    ( Node.HtmlLabel
                        ( transitionLabel
                            transitionId
                            transition
                        )
                    )
                , Node.Shape Node.None
                ]
          )

  all :: Array Definition
  all = renderTransitions snapshot.transitions

  renderTransitions :: Transitions -> Array Definition
  renderTransitions transitions =
    transitions
      |> mapMaybeWithKey renderTransition
      |> MS.values
      |> List.toUnfoldable
      |> bindFlipped
          ( \definitions ->
              [ definitions.in
              , definitions.out
              ]
          )

  renderTransition :: TransitionId -> Transition -> Maybe { in :: Definition, out :: Definition }
  renderTransition transitionId transition =
    Just
      { in:
        escape transition.origin
          =*> escape transitionId
          $ [ Edge.ArrowHead Edge.None ]
      , out: escape transitionId ==> escape transition.exit
      }

  escape ::
    String -> String
  escape = replaceAll (P.Pattern "-") (Replacement "")

  colorForNodeName :: State -> Color.Color
  colorForNodeName nodeName =
    if (escape snapshot.currentState) == nodeName then
      Color.rgb 0 0 255
    else
      Color.black

  transitionLabel :: TransitionId -> Transition -> String
  transitionLabel transitionId transition =
    """
      <<TABLE BORDER="0" CELLBORDER="1" CELLSPACING="0">
        <TR><TD BGCOLOR="gray">Transition</TD></TR>
        <TR><TD BGCOLOR="white">"""
      <> transitionId
      <> """</TD></TR>
        <TR><TD BGCOLOR="gray">Roles</TD></TR>"""
      <> renderRoles transition.validation.checkRoles
      <> """
        <TR><TD BGCOLOR="gray">Required</TD></TR>"""
      <> renderFieldNames transition.validation.requiredFields
      <> """<TR><TD BGCOLOR="gray">Optional</TD></TR>"""
      <> renderFieldNames transition.validation.optionalFields
      <> """</TABLE>>"""

  renderRoles :: List.List RoleTag -> String
  renderRoles roles =
    if List.null roles then
      ""
    else
      """<TR><TD BGCOLOR="white">"""
        <> intercalate """</TD></TR><TR><TD BGCOLOR="white">""" roles
        <> """</TD></TR>"""

  renderFieldNames :: List.List FieldName -> String
  renderFieldNames fieldNames =
    if List.null fieldNames then
      ""
    else
      """
      <TR><TD BGCOLOR="white">"""
        <> intercalate """</TD></TR><TR><TD BGCOLOR="white">""" fieldNames
        <> """</TD></TR>"""
