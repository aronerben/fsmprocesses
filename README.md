# FSM Processes

## Paper

The latest version of the paper can be downloaded here:

[latest version](https://gitlab.com/olodnad/fsmprocesses/-/jobs/artifacts/master/download?job=documentation)

## Simulator

[Simulator](https://olodnad.gitlab.io/fsmprocesses/implementation/)

## Documentation

- [Full Documentation](https://olodnad.gitlab.io/fsmprocesses/documentation/)
- [Processes.Model.Types](https://olodnad.gitlab.io/fsmprocesses/documentation/Processes.Model.Types.html)
- [Processes.Model.UpdateSystem](https://olodnad.gitlab.io/fsmprocesses/documentation/Processes.Model.UpdateSystem.html)
- [Processes.Form.Types](https://olodnad.gitlab.io/fsmprocesses/documentation/Processes.Form.Types.html)

## Development

drop into nix shell:
    nix-shell


install dependencies

    spago install
    npm i

and build the code

    spago build

or start two terminals for auto reloading/dev:

    parcel index.html

    spago build --watch
